table! {
    authentications (id) {
        id -> Uuid,
        user_id -> Uuid,
        authentication_type -> Varchar,
        authentication_key -> Varchar,
    }
}

table! {
    refresh_tokens (id) {
        id -> Uuid,
        user_id -> Uuid,
        token -> Text,
        created_at -> Timestamp,
        valid_until -> Timestamp,
    }
}

table! {
    users (id) {
        id -> Uuid,
        username -> Varchar,
        email -> Varchar,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

joinable!(authentications -> users (user_id));
joinable!(refresh_tokens -> users (user_id));

allow_tables_to_appear_in_same_query!(authentications, refresh_tokens, users,);
