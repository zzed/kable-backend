#![feature(result_flattening)]
#![feature(is_some_with)]

mod config;
mod dto;
mod models;
mod routes;
mod schema;

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;
#[macro_use]
extern crate log;
extern crate derive_more;
extern crate env_logger;

use actix_cors::Cors;
use crate::config::ApplicationConfig;
use crate::routes::v1;
use actix_web::middleware::{Logger, NormalizePath, TrailingSlash};
use actix_web::{get, web, App, HttpResponse, HttpServer, Responder};
use actix_web::http::header;
use diesel::r2d2::{ConnectionManager, Pool};
use diesel::PgConnection;
use figment::{Profile, Provider};
use log::Level;

embed_migrations!("./migrations");
pub type Database = Pool<ConnectionManager<PgConnection>>;
pub const JWT_COOKIE: &str = "jwt";
pub const REFRESH_COOKIE: &str = "refresh";

#[cfg(test)]
mod tests;
mod utils;

#[get("/")]
async fn hello() -> impl Responder {
    HttpResponse::Ok().body("Hello world!")
}

fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(web::scope("/api/v1").configure(v1::config));
}

pub struct State {
    pub db: Database,
    pub config: ApplicationConfig,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv::dotenv().ok();
    env_logger::init_from_env(env_logger::Env::new().default_filter_or("debug"));

    let app_config: ApplicationConfig = ApplicationConfig::figment()
        .extract()
        .expect("Could not compile configuration");

    let conn_spec = app_config.database_url.clone();
    let manager = ConnectionManager::<PgConnection>::new(conn_spec);
    let pool = Pool::builder()
        .build(manager)
        .expect("Failed to create pool.");

    embedded_migrations::run(&pool.get().expect("Failed to get connection"))
        .expect("Failed to run migration");

    let host = app_config.host.clone();
    let port = app_config.port;

    log!(Level::Info, "Started web app at {}:{}", host, port);

    HttpServer::new(move || {

        let cors = if cfg!(debug_assertions) {
            Cors::permissive()
        } else {
            Cors::default()
                .supports_credentials()
                .allowed_methods(vec!["GET", "POST", "PUT", "DELETE"])
                .allowed_headers(vec![header::AUTHORIZATION, header::ACCEPT])
                .allowed_header(header::CONTENT_TYPE)
                .max_age(3600)
        };

        App::new()
            .app_data(web::Data::new(State {
                db: pool.clone(),
                config: app_config.clone(),
            }))
            .wrap(cors)
            .wrap(Logger::default())
            .wrap(NormalizePath::new(TrailingSlash::Trim))
            .configure(config)
    })
    .bind((host, port))?
    .run()
    .await
}
