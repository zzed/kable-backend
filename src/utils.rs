use crate::models::user::User;
use actix_web::http::header;
use actix_web::http::header::HeaderMap;
use chrono::{DateTime, Duration, Utc};
use derive_more::Display;
use jsonwebtoken::{Algorithm, DecodingKey, Validation};
use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::str::FromStr;
use uuid::Uuid;

pub trait GetRequiredJWTClaims {
    fn get_sub(&self) -> String;
    fn get_exp(&self) -> usize;
    fn get_iat(&self) -> usize;
}

pub trait SetJWTClaims {
    fn get_claims(user: &User, timestamp: DateTime<Utc>, duration: Duration) -> Self;
}

#[derive(Serialize, Deserialize, Debug)]
pub struct RefreshTokenClaims {
    pub sub: String,
    pub exp: usize,
    pub iat: usize,
}

impl SetJWTClaims for RefreshTokenClaims {
    fn get_claims(user: &User, timestamp: DateTime<Utc>, duration: Duration) -> Self {
        Self {
            sub: user.id.to_string(),
            iat: timestamp.timestamp() as usize,
            exp: (timestamp + duration).timestamp() as usize,
        }
    }
}

#[allow(dead_code)]
pub fn generate_random_string(length: usize) -> String {
    let rand_string: String = thread_rng()
        .sample_iter(&Alphanumeric)
        .take(length)
        .map(char::from)
        .collect();

    rand_string
}

#[derive(Serialize, Deserialize, Debug)]
pub struct UserTokenClaims {
    pub sub: String,
    pub exp: usize,
    pub iat: usize,
}

impl SetJWTClaims for UserTokenClaims {
    fn get_claims(user: &User, timestamp: DateTime<Utc>, duration: Duration) -> Self {
        Self {
            sub: user.id.to_string(),
            iat: timestamp.timestamp() as usize,
            exp: (timestamp + duration).timestamp() as usize,
        }
    }
}

impl GetRequiredJWTClaims for UserTokenClaims {
    fn get_sub(&self) -> String {
        self.sub.to_string()
    }
    fn get_exp(&self) -> usize {
        self.exp
    }
    fn get_iat(&self) -> usize {
        self.iat
    }
}

#[derive(Display, Debug)]
pub enum IsUserLoggedInError {
    NoAuthHeader,
    JwtError(jsonwebtoken::errors::Error),
    UuidError(uuid::Error),
    GenericError,
}

pub fn is_user_logged_in<Claims>(
    headers: &HeaderMap,
    decoding_key: &DecodingKey,
    algorithm: Algorithm,
) -> Result<Uuid, IsUserLoggedInError>
where
    Claims: GetRequiredJWTClaims + DeserializeOwned,
{
    let jwt: String = headers
        .get(header::AUTHORIZATION)
        .ok_or(IsUserLoggedInError::NoAuthHeader)?
        .to_str()
        .map_err(|_| IsUserLoggedInError::GenericError)?
        .split("Bearer ")
        .collect();

    let claims: Claims =
        jsonwebtoken::decode::<Claims>(&jwt, decoding_key, &Validation::new(algorithm))
            .map_err(IsUserLoggedInError::JwtError)?
            .claims;

    let uuid = Uuid::from_str(&claims.get_sub()).map_err(IsUserLoggedInError::UuidError)?;

    Ok(uuid)
}
