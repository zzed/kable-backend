use actix_web::web::ServiceConfig;
use actix_web::{get, HttpResponse, Responder};

#[get("")]
async fn ping() -> impl Responder {
    HttpResponse::Ok().body("Pong!")
}

pub fn config(cfg: &mut ServiceConfig) {
    cfg.service(ping);
}
