use crate::dto::user::{CreateUserDto, LoginUserDto, PublicUserDto};
use crate::models::refresh_token::actions::insert_refresh_token;
use crate::models::refresh_token::RefreshTokenInsertion;
use crate::models::user::actions::{check_credentials, find_by_id, insert_user};
use crate::models::user::types::{UserInsertionError, UserLoginError};
use crate::utils::{is_user_logged_in, RefreshTokenClaims, SetJWTClaims, UserTokenClaims};
use crate::v1::user::types::UserDataFetchError;
use crate::State;
use actix_web::http::header;
use actix_web::http::header::HeaderValue;
use actix_web::web::Json;
use actix_web::{get, post, web, web::ServiceConfig, HttpRequest, HttpResponse};
use chrono::{Duration, Utc};
use jsonwebtoken::{Algorithm, DecodingKey, EncodingKey, Header};
use std::str::FromStr;
use uuid::Uuid;

pub mod test;
pub mod types;

#[post("")]
async fn register_user(
    user_to_create: Json<CreateUserDto>,
    state: web::Data<State>,
) -> Result<HttpResponse, UserInsertionError> {
    let db = state.db.get().expect("Could not connect to database.");
    let password_salt = state.config.password_salt.clone();

    let create_user_dto = user_to_create.0;

    info!("Registering new user.");

    let created_user_uuid = web::block(move || insert_user(&db, password_salt, create_user_dto))
        .await
        .map_err(|_| UserInsertionError::GenericError)
        .flatten()?;

    info!("Created user with UUID: {}", created_user_uuid);

    let location_header = HeaderValue::from_str(&format!("/api/v1/user/{}", created_user_uuid))
        .expect("Bad location header value");

    Ok(HttpResponse::Created()
        .append_header((header::LOCATION, location_header))
        .finish())
}

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct LoginResponse {
    jwt_token: String,
    refresh_token: String,
}

#[post("/login/password")]
async fn login(
    login_data: Json<LoginUserDto>,
    state: web::Data<State>,
) -> Result<HttpResponse, UserLoginError> {
    let login_data = login_data.0;
    let db = state.db.get().expect("Could not connect to database.");
    let jwt_secret = state.config.clone().jwt_secret;

    info!("Attempting to log in.");
    let current_timestamp = Utc::now();
    let jwt_duration = Duration::minutes(5);
    let refresh_token_duration = Duration::days(1);

    let user = check_credentials(&db, login_data.username, login_data.password)?;

    let jwt_claims = UserTokenClaims::get_claims(&user, current_timestamp, jwt_duration);

    let jwt = jsonwebtoken::encode(
        &Header::default(),
        &jwt_claims,
        &EncodingKey::from_secret(jwt_secret.as_bytes()),
    )
    .expect("Could not encode JWT.");

    let refresh_token_claims =
        RefreshTokenClaims::get_claims(&user, current_timestamp, refresh_token_duration);

    let refresh_token = jsonwebtoken::encode(
        &Header::default(),
        &refresh_token_claims,
        &EncodingKey::from_secret(jwt_secret.as_bytes()),
    )
    .expect("Could not encode refresh token.");

    let refresh_token = insert_refresh_token(
        &db,
        RefreshTokenInsertion {
            token: &refresh_token,
            user: Box::new(user),
        },
        current_timestamp,
        refresh_token_duration,
    )
    .map_err(|err| {
        error!("{:?}", err);
        UserLoginError::GenericError
    })?;

    Ok(HttpResponse::Ok().json(LoginResponse {
        jwt_token: jwt,
        refresh_token,
    }))
}

#[get("/")]
async fn user_data(
    req: HttpRequest,
    state: web::Data<State>,
) -> Result<HttpResponse, UserDataFetchError> {
    let db = state.db.get().expect("Could not connect to database.");
    let jwt_secret = state.config.clone().jwt_secret;

    let user_id = is_user_logged_in::<UserTokenClaims>(
        req.headers(),
        &DecodingKey::from_secret(jwt_secret.as_bytes()),
        Algorithm::HS256,
    )?;

    info!("Attempting to log in.");

    let user = find_by_id(&db, user_id).map_err(UserDataFetchError::UserFindError)?;

    let public_user = PublicUserDto::from(user);

    Ok(HttpResponse::Ok().json(public_user))
}

#[get("/{user_id}")]
async fn get_user_data(
    req: HttpRequest,
    state: web::Data<State>,
    path: web::Path<String>,
) -> Result<HttpResponse, UserDataFetchError> {
    let db = state.db.get().expect("Could not connect to database.");
    let user_id = Uuid::from_str(path.into_inner().as_str())
        .map_err(|_| UserDataFetchError::UuidCreationError)?;
    let jwt_secret = state.config.clone().jwt_secret;

    is_user_logged_in::<UserTokenClaims>(
        req.headers(),
        &DecodingKey::from_secret(jwt_secret.as_bytes()),
        Algorithm::HS256,
    )?;

    let user = find_by_id(&db, user_id).map_err(UserDataFetchError::UserFindError)?;

    let public_user = PublicUserDto::from(user);

    Ok(HttpResponse::Ok().json(public_user))
}

pub fn config(cfg: &mut ServiceConfig) {
    cfg.service(login)
        .service(user_data)
        .service(register_user)
        .service(get_user_data);
}
