#[cfg(test)]
mod register_tests {
    use super::super::*;
    use crate::dto::user::*;
    use crate::tests::test_context::TestContext;
    use crate::{ApplicationConfig, State};
    use actix_web::dev::ServiceResponse;
    use actix_web::http::StatusCode;
    use actix_web::middleware::{NormalizePath, TrailingSlash};
    use actix_web::{test, web, App};
    use diesel::r2d2::{ConnectionManager, Pool};
    use diesel::PgConnection;
    use serde_json::json;
    use std::str::FromStr;
    use uuid::Uuid;

    #[actix_web::test]
    async fn creates_user_and_returns_public_user_data() {
        let app_config: ApplicationConfig = ApplicationConfig::figment()
            .extract()
            .expect("Could not create application config");

        let db_context = TestContext::new(
            &app_config.test_database_url,
            "creates_user_and_returns_public_user_data",
        );

        let manager = ConnectionManager::<PgConnection>::new(db_context.conn_spec.clone());
        let pool = Pool::builder()
            .build(manager)
            .expect("Failed to create pool.");

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(State {
                    db: pool.clone(),
                    config: app_config,
                }))
                .wrap(NormalizePath::new(TrailingSlash::Trim))
                .service(web::scope("/api/v1").service(web::scope("/user").configure(config))),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/api/v1/user")
            .set_json(CreateUserDto {
                username: "foobar".to_string(),
                email: "foobar@example.com".to_string(),
                password: "Tarball".to_string(),
            })
            .to_request();

        let response: ServiceResponse = test::call_service(&app, req).await;
        assert_eq!(response.status(), StatusCode::CREATED);

        let headers = response.headers();
        let location = headers
            .get(header::LOCATION)
            .expect("There is no location header.")
            .to_str()
            .unwrap();
        let user_guid = location.split('/').last().unwrap();

        assert!(location.contains("/api/v1/user"));
        assert_eq!(response.status(), StatusCode::CREATED);
        assert!(Uuid::from_str(user_guid).is_ok());
    }

    #[actix_web::test]
    async fn if_user_exists_show_forbidden() {
        let app_config: ApplicationConfig = ApplicationConfig::figment()
            .extract()
            .expect("Could not create application config");

        let db_context = TestContext::new(
            &app_config.test_database_url,
            "if_user_exists_show_forbidden",
        );

        let manager = ConnectionManager::<PgConnection>::new(db_context.conn_spec.clone());
        let pool = Pool::builder()
            .build(manager)
            .expect("Failed to create pool.");

        let conn = pool.get().expect("Could not get connection to database");
        let password_salt = app_config.clone().password_salt;

        web::block(move || {
            insert_user(
                &conn,
                password_salt,
                CreateUserDto {
                    username: "foobar".to_string(),
                    email: "foobar@example.com".to_string(),
                    password: "Tarball".to_string(),
                },
            )
            .expect("Could not create user.")
        })
        .await
        .expect("Could not run database task");

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(State {
                    db: pool.clone(),
                    config: app_config,
                }))
                .wrap(NormalizePath::new(TrailingSlash::Trim))
                .service(web::scope("/api/v1").service(web::scope("/user").configure(config))),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/api/v1/user")
            .set_json(CreateUserDto {
                username: "foobar".to_string(),
                email: "foobar@example.com".to_string(),
                password: "Tarball".to_string(),
            })
            .to_request();

        let response: ServiceResponse = test::call_service(&app, req).await;
        assert_eq!(response.status(), StatusCode::FORBIDDEN);
    }

    #[actix_web::test]
    async fn malformed_body_shows_bad_request() {
        let app_config: ApplicationConfig = ApplicationConfig::figment()
            .extract()
            .expect("Could not create application config");

        let db_context = TestContext::new(
            &app_config.test_database_url,
            "malformed_body_shows_bad_request",
        );

        let manager = ConnectionManager::<PgConnection>::new(db_context.conn_spec.clone());
        let pool = Pool::builder()
            .build(manager)
            .expect("Failed to create pool.");

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(State {
                    db: pool.clone(),
                    config: app_config,
                }))
                .wrap(NormalizePath::new(TrailingSlash::Trim))
                .service(web::scope("/api/v1").service(web::scope("/user").configure(config))),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/api/v1/user")
            .set_json(json!({ "foo": "bar" }))
            .to_request();

        let response: ServiceResponse = test::call_service(&app, req).await;
        assert_eq!(response.status(), StatusCode::BAD_REQUEST);
    }
}

#[cfg(test)]
mod login_tests {
    use super::super::*;
    use crate::dto::user::*;
    use crate::tests::test_context::TestContext;
    use crate::utils::{RefreshTokenClaims, UserTokenClaims};
    use crate::{ApplicationConfig, State};
    use actix_web::middleware::{NormalizePath, TrailingSlash};
    use actix_web::{http::StatusCode, test, web, App};
    use chrono::TimeZone;
    use diesel::r2d2::{ConnectionManager, Pool};
    use diesel::PgConnection;
    use jsonwebtoken::{Algorithm, DecodingKey, Validation};

    #[actix_web::test]
    async fn correct_login_returns_jwt_in_body() {
        let app_config: ApplicationConfig = ApplicationConfig::figment()
            .select("release")
            .extract()
            .expect("Could not create application config");

        let db_context = TestContext::new(
            &app_config.test_database_url,
            "correct_login_returns_jwt_in_cookie",
        );

        let manager = ConnectionManager::<PgConnection>::new(db_context.conn_spec.clone());
        let pool = Pool::builder()
            .build(manager)
            .expect("Failed to create pool.");

        let conn = pool.get().expect("Could not get connection to database");
        let password_salt = app_config.clone().password_salt;

        let user_insertion = web::block(move || {
            insert_user(
                &conn,
                password_salt,
                CreateUserDto {
                    username: "foobar".to_string(),
                    email: "foobar@example.com".to_string(),
                    password: "hunter42".to_string(),
                },
            )
            .expect("Could not create user.")
        })
        .await
        .expect("Could not run database task");

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(State {
                    db: pool.clone(),
                    config: app_config.clone(),
                }))
                .wrap(NormalizePath::new(TrailingSlash::Trim))
                .configure(config),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/login/password")
            .set_json(LoginUserDto {
                username: "foobar".to_string(),
                password: "hunter42".to_string(),
            })
            .to_request();

        let response = test::call_service(&app, req).await;

        assert_eq!(response.status(), StatusCode::OK);

        let body: LoginResponse = test::read_body_json(response).await;

        let jwt = jsonwebtoken::decode::<UserTokenClaims>(
            &body.jwt_token,
            &DecodingKey::from_secret(app_config.jwt_secret.as_bytes()),
            &Validation::new(Algorithm::HS256),
        )
        .expect("Could not decode JWT.");

        let jwt_start = Utc.timestamp(jwt.claims.iat as i64, 0);
        let jwt_end = Utc.timestamp(jwt.claims.exp as i64, 0);

        assert_eq!((jwt_end - jwt_start).num_minutes(), 5);
        assert_eq!(jwt.claims.sub, user_insertion.to_string());

        // ! END JWT COOKIE !

        // ! REFRESH COOKIE !

        let refresh_jwt = jsonwebtoken::decode::<RefreshTokenClaims>(
            &body.refresh_token,
            &DecodingKey::from_secret(app_config.jwt_secret.as_bytes()),
            &Validation::new(Algorithm::HS256),
        )
        .expect("Could not decode JWT.");

        let refresh_start = Utc.timestamp(refresh_jwt.claims.iat as i64, 0);
        let refresh_end = Utc.timestamp(refresh_jwt.claims.exp as i64, 0);

        assert_eq!(refresh_jwt.claims.sub, user_insertion.to_string());
        assert_eq!((refresh_end - refresh_start).num_days(), 1);

        // ! END REFRESH COOKIE !
    }

    #[actix_web::test]
    async fn incorrect_password_shows_forbidden() {
        let app_config: ApplicationConfig = ApplicationConfig::figment()
            .extract()
            .expect("Could not create application config");

        let db_context = TestContext::new(
            &app_config.test_database_url,
            "incorrect_password_shows_forbidden",
        );

        let manager = ConnectionManager::<PgConnection>::new(db_context.conn_spec.clone());
        let pool = Pool::builder()
            .build(manager)
            .expect("Failed to create pool.");

        let conn = pool.get().expect("Could not get connection to database");
        let password_salt = app_config.clone().password_salt;

        web::block(move || {
            insert_user(
                &conn,
                password_salt,
                CreateUserDto {
                    username: "foobar".to_string(),
                    email: "foobar@example.com".to_string(),
                    password: "hunter42".to_string(),
                },
            )
            .expect("Could not create user.")
        })
        .await
        .expect("Could not run database task");

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(State {
                    db: pool.clone(),
                    config: app_config,
                }))
                .wrap(NormalizePath::new(TrailingSlash::Trim))
                .service(web::scope("/api/v1").service(web::scope("/user").configure(config))),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/api/v1/user/login/password")
            .set_json(LoginUserDto {
                username: "foobar".to_string(),
                password: "hunter43".to_string(),
            })
            .to_request();

        let response = test::call_service(&app, req).await;

        assert_eq!(response.status(), StatusCode::FORBIDDEN);
    }

    #[actix_web::test]
    async fn no_user_shows_not_found() {
        let app_config: ApplicationConfig = ApplicationConfig::figment()
            .extract()
            .expect("Could not create application config");

        let db_context = TestContext::new(&app_config.test_database_url, "no_user_shows_not_found");

        let manager = ConnectionManager::<PgConnection>::new(db_context.conn_spec.clone());
        let pool = Pool::builder()
            .build(manager)
            .expect("Failed to create pool.");

        let conn = pool.get().expect("Could not get connection to database");
        let password_salt = app_config.clone().password_salt;

        web::block(move || {
            insert_user(
                &conn,
                password_salt,
                CreateUserDto {
                    username: "foobar".to_string(),
                    email: "foobar@example.com".to_string(),
                    password: "hunter42".to_string(),
                },
            )
            .expect("Could not create user.")
        })
        .await
        .expect("Could not run database task");

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(State {
                    db: pool.clone(),
                    config: app_config,
                }))
                .wrap(NormalizePath::new(TrailingSlash::Trim))
                .service(web::scope("/api/v1").service(web::scope("/user").configure(config))),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/api/v1/user/login/password")
            .set_json(LoginUserDto {
                username: "bazdaw".to_string(),
                password: "hunter42".to_string(),
            })
            .to_request();

        let response = test::call_service(&app, req).await;

        assert_eq!(response.status(), StatusCode::NOT_FOUND);
    }
}

#[cfg(test)]
mod user_data_endpoint_tests {
    use super::super::*;
    use crate::dto::user::*;
    use crate::tests::test_context::TestContext;
    use crate::{ApplicationConfig, State};
    use actix_web::middleware::{NormalizePath, TrailingSlash};
    use actix_web::{http::StatusCode, test, web, App};
    use diesel::r2d2::{ConnectionManager, Pool};
    use diesel::PgConnection;

    #[actix_web::test]
    async fn logged_in_account_can_get_information_about_themselves() {
        let app_config: ApplicationConfig = ApplicationConfig::figment()
            .select("release")
            .extract()
            .expect("Could not create application config");

        let db_context = TestContext::new(
            &app_config.test_database_url,
            "logged_in_account_can_get_information_about_themselves",
        );

        let manager = ConnectionManager::<PgConnection>::new(db_context.conn_spec.clone());
        let pool = Pool::builder()
            .build(manager)
            .expect("Failed to create pool.");

        let conn = pool.get().expect("Could not get connection to database");
        let password_salt = app_config.clone().password_salt;

        let user_insertion = web::block(move || {
            insert_user(
                &conn,
                password_salt,
                CreateUserDto {
                    username: "foobar".to_string(),
                    email: "foobar@example.com".to_string(),
                    password: "hunter42".to_string(),
                },
            )
            .expect("Could not create user.")
        })
        .await
        .expect("Could not run database task");

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(State {
                    db: pool.clone(),
                    config: app_config.clone(),
                }))
                .wrap(NormalizePath::new(TrailingSlash::Trim))
                .configure(config),
        )
        .await;

        let login_req = test::TestRequest::post()
            .uri("/login/password")
            .set_json(LoginUserDto {
                username: "foobar".to_string(),
                password: "hunter42".to_string(),
            })
            .to_request();

        let login_response = test::call_service(&app, login_req).await;
        let jwt: LoginResponse = test::read_body_json(login_response).await;

        let user_data_req = test::TestRequest::get()
            .uri("/")
            .insert_header((header::AUTHORIZATION, jwt.jwt_token))
            .to_request();

        let user_data_response = test::call_service(&app, user_data_req).await;

        assert_eq!(user_data_response.status(), StatusCode::OK);

        let data: PublicUserDto = test::read_body_json(user_data_response).await;

        assert_eq!(data.id, user_insertion);
        assert_eq!(data.username, "foobar");
        assert_eq!(data.email, "foobar@example.com");
    }

    #[actix_web::test]
    async fn not_logged_in_user_shows_unauthorized() {
        let app_config: ApplicationConfig = ApplicationConfig::figment()
            .select("release")
            .extract()
            .expect("Could not create application config");

        let db_context = TestContext::new(
            &app_config.test_database_url,
            "not_logged_in_user_shows_unauthorized",
        );

        let manager = ConnectionManager::<PgConnection>::new(db_context.conn_spec.clone());
        let pool = Pool::builder()
            .build(manager)
            .expect("Failed to create pool.");

        let conn = pool.get().expect("Could not get connection to database");
        let password_salt = app_config.clone().password_salt;

        web::block(move || {
            insert_user(
                &conn,
                password_salt,
                CreateUserDto {
                    username: "foobar".to_string(),
                    email: "foobar@example.com".to_string(),
                    password: "hunter42".to_string(),
                },
            )
            .expect("Could not create user.")
        })
        .await
        .expect("Could not run database task");

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(State {
                    db: pool.clone(),
                    config: app_config.clone(),
                }))
                .wrap(NormalizePath::new(TrailingSlash::Trim))
                .configure(config),
        )
        .await;

        let req = test::TestRequest::get().uri("/").to_request();

        let response = test::call_service(&app, req).await;
        assert_eq!(response.status(), StatusCode::UNAUTHORIZED);
    }

    #[actix_web::test]
    async fn if_logged_in_can_access_other_users() {
        let app_config: ApplicationConfig = ApplicationConfig::figment()
            .select("release")
            .extract()
            .expect("Could not create application config");

        let db_context = TestContext::new(
            &app_config.test_database_url,
            "if_logged_in_can_access_other_users",
        );

        let manager = ConnectionManager::<PgConnection>::new(db_context.conn_spec.clone());
        let pool = Pool::builder()
            .build(manager)
            .expect("Failed to create pool.");

        let conn = pool.get().expect("Could not get connection to database");
        let password_salt = app_config.clone().password_salt;

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(State {
                    db: pool.clone(),
                    config: app_config.clone(),
                }))
                .wrap(NormalizePath::new(TrailingSlash::Trim))
                .service(web::scope("/api/v1").service(web::scope("/user").configure(config))),
        )
        .await;

        web::block(move || {
            insert_user(
                &conn,
                password_salt,
                CreateUserDto {
                    username: "foobar".to_string(),
                    email: "foobar@example.com".to_string(),
                    password: "hunter42".to_string(),
                },
            )
            .expect("Could not create user.")
        })
        .await
        .expect("Could not run database task");

        let req = test::TestRequest::post()
            .uri("/api/v1/user/login/password")
            .set_json(LoginUserDto {
                username: "foobar".to_string(),
                password: "hunter42".to_string(),
            })
            .to_request();

        let response = test::call_service(&app, req).await;

        assert_eq!(response.status(), StatusCode::OK);

        let login_body: LoginResponse = test::read_body_json(response).await;

        let user_jwt = login_body.jwt_token;

        let new_user = CreateUserDto {
            username: "barbaz".to_string(),
            email: "barbaz@example.com".to_string(),
            password: "Tarball".to_string(),
        };

        let register_request = test::TestRequest::post()
            .uri("/api/v1/user")
            .set_json(new_user)
            .to_request();

        let register_response = test::call_service(&app, register_request).await;
        let new_user_location = register_response.headers().get("location");

        assert_eq!(register_response.status(), StatusCode::CREATED);
        assert!(new_user_location.is_some());

        let new_user_location = new_user_location.unwrap().to_str().unwrap();

        let user_data_req = test::TestRequest::get()
            .insert_header((header::AUTHORIZATION, format!("Bearer {}", user_jwt)))
            .uri(new_user_location)
            .to_request();

        let user_data_response = test::call_service(&app, user_data_req).await;
        let user_data_body: PublicUserDto = test::read_body_json(user_data_response).await;
        let user_id: String = new_user_location.split("/api/v1/user/").collect();

        assert_eq!(user_data_body.email, "barbaz@example.com");
        assert_eq!(user_data_body.username, "barbaz");
        assert_eq!(user_data_body.id, Uuid::from_str(&user_id).unwrap());
    }
}
