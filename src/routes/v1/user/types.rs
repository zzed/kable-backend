use crate::models::user::types::UserFindError;
use crate::utils::IsUserLoggedInError;
use actix_web::http::StatusCode;
use actix_web::{HttpResponse, ResponseError};
use derive_more::Error;
use jsonwebtoken::errors::ErrorKind;
use std::fmt::{Display, Formatter};

#[derive(Debug, Error)]
pub enum UserDataFetchError {
    NoAuthHeader,      // HttpResponse::Unauthorized().finish()
    UuidCreationError, // HttpResponse::InternalServerError().body("There is an invalid user ID.")
    ExpiredSignature,  // HttpResponse::BadRequest().body("You are using an expired JWT.")
    JwtDecodeError,    // HttpResponse::InternalServerError().finish()
    GenericError,
    UserFindError(UserFindError),
}

impl Display for UserDataFetchError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                UserDataFetchError::UuidCreationError => "Invalid user ID",
                UserDataFetchError::ExpiredSignature => "You are using an expired JWT.",
                _ => "",
            }
        )
    }
}

impl ResponseError for UserDataFetchError {
    fn status_code(&self) -> StatusCode {
        match self {
            UserDataFetchError::NoAuthHeader => StatusCode::UNAUTHORIZED,
            UserDataFetchError::UuidCreationError => StatusCode::INTERNAL_SERVER_ERROR,
            UserDataFetchError::ExpiredSignature => StatusCode::FORBIDDEN,
            UserDataFetchError::JwtDecodeError => StatusCode::BAD_REQUEST,
            UserDataFetchError::UserFindError(user_find_error) => user_find_error.status_code(),
            _ => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }

    fn error_response(&self) -> HttpResponse {
        error!(
            "An error in the user data fetch action has occured: {}",
            self.to_string()
        );
        match self {
            UserDataFetchError::UserFindError(user_find_error) => user_find_error.error_response(),
            _ => HttpResponse::build(self.status_code()).body(self.to_string()),
        }
    }
}

impl From<IsUserLoggedInError> for UserDataFetchError {
    fn from(fn_error: IsUserLoggedInError) -> Self {
        match fn_error {
            IsUserLoggedInError::NoAuthHeader => UserDataFetchError::NoAuthHeader,
            IsUserLoggedInError::JwtError(jwt_error) => match jwt_error.kind() {
                ErrorKind::ExpiredSignature => UserDataFetchError::ExpiredSignature,
                _ => UserDataFetchError::JwtDecodeError,
            },
            IsUserLoggedInError::UuidError(_) => UserDataFetchError::UuidCreationError,
            IsUserLoggedInError::GenericError => UserDataFetchError::GenericError,
        }
    }
}
