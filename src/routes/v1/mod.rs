use actix_web::web;
use actix_web::web::ServiceConfig;

mod ping;
mod user;

pub fn config(cfg: &mut ServiceConfig) {
    cfg.service(web::scope("/ping").configure(ping::config))
        .service(web::scope("/user").configure(user::config));
}
