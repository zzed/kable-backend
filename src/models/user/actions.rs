use super::types::{UserInsertionError, UserInsertionResult};
use crate::diesel::prelude::*;
use crate::diesel::result::{DatabaseErrorKind, Error};
use crate::diesel::{
    r2d2::{ConnectionManager, PooledConnection},
    PgConnection,
};
use crate::dto::user::CreateUserDto;
use crate::models::user::types::{UserFindError, UserFindResult, UserLoginError, UserLoginResult};
use crate::models::user::{Authentication, NewAuthentication, NewUser, User};
use crate::schema::authentications::dsl::authentications;
use argon2::Config;
use uuid::Uuid;

type Connection = PooledConnection<ConnectionManager<PgConnection>>;

pub fn insert_user(
    db: &Connection,
    password_salt: String,
    create_user_dto: CreateUserDto,
) -> UserInsertionResult {
    use crate::schema::users::dsl::*;

    let hashed_password = argon2::hash_encoded(
        create_user_dto.password.as_bytes(),
        password_salt.as_bytes(),
        &Config::default(),
    )
    .expect("Could not hash password");

    let result_user_uuid_creation = diesel::insert_into(users)
        .values(NewUser {
            username: create_user_dto.username,
            email: create_user_dto.email,
        })
        .returning(id)
        .get_result::<Uuid>(db)
        .map_err(|err| match err {
            Error::DatabaseError(DatabaseErrorKind::UniqueViolation, _) => {
                UserInsertionError::UserAlreadyExists
            }
            _ => UserInsertionError::UserAlreadyExists,
        })?;

    diesel::insert_into(authentications)
        .values(NewAuthentication {
            user_id: result_user_uuid_creation,
            authentication_type: "password".to_string(),
            authentication_key: hashed_password,
        })
        .execute(db)
        .expect("Could not insert password");

    Ok(result_user_uuid_creation)
}

pub fn find_by_username(db: &Connection, user_username: String) -> UserFindResult {
    use crate::schema::users::dsl::*;

    users
        .filter(username.eq(user_username))
        .first::<User>(db)
        .map_err(|err| match err {
            Error::NotFound => UserFindError::UserNotFound,
            _ => UserFindError::GenericError,
        })
}

pub fn check_credentials(
    db: &Connection,
    user_username: String,
    user_password: String,
) -> UserLoginResult {
    let user = find_by_username(db, user_username).map_err(|err| match err {
        UserFindError::UserNotFound => UserLoginError::UserNotFound,
        _ => UserLoginError::GenericError,
    })?;

    let hashed_password: Authentication = Authentication::belonging_to(&user)
        .first::<Authentication>(db)
        .map_err(|_| UserLoginError::GenericError)?;

    let hash_check =
        argon2::verify_encoded(&hashed_password.authentication_key, user_password.as_ref());

    match hash_check {
        Ok(true) => Ok(user),
        Ok(false) => Err(UserLoginError::IncorrectCredentials),
        Err(_) => Err(UserLoginError::GenericError),
    }
}

pub fn find_by_id(db: &Connection, user_id: Uuid) -> UserFindResult {
    use crate::schema::users::dsl::*;

    users
        .filter(id.eq(user_id))
        .first::<User>(db)
        .map_err(|err| match err {
            Error::NotFound => UserFindError::UserNotFound,
            _ => UserFindError::GenericError,
        })
}
