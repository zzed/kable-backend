use crate::schema::authentications;
use crate::schema::users;
use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

pub mod actions;
pub mod types;

#[derive(Queryable, Serialize, Deserialize, Debug, PartialEq, Eq, Identifiable)]
pub struct User {
    pub id: Uuid,
    pub username: String,
    pub email: String,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[derive(Insertable)]
#[table_name = "users"]
pub struct NewUser {
    pub username: String,
    pub email: String,
}

#[derive(Identifiable, Queryable, Associations, Serialize, Deserialize, PartialEq, Eq, Debug)]
#[belongs_to(User)]
#[table_name = "authentications"]
pub struct Authentication {
    pub id: Uuid,
    pub user_id: Uuid,
    pub authentication_type: String,
    pub authentication_key: String,
}

#[derive(Insertable)]
#[table_name = "authentications"]
pub struct NewAuthentication {
    pub user_id: Uuid,
    pub authentication_type: String,
    pub authentication_key: String,
}
