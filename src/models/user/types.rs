use crate::models::user::User;
use actix_web::http::StatusCode;
use actix_web::{HttpResponse, ResponseError};
use derive_more::Error;
use std::fmt::{Display, Formatter};
use uuid::Uuid;

#[derive(Debug, Error)]
pub enum UserInsertionError {
    UserAlreadyExists,
    GenericError,
}

impl Display for UserInsertionError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                UserInsertionError::UserAlreadyExists => "User already exists.",
                _ => "",
            }
        )
    }
}

impl ResponseError for UserInsertionError {
    fn status_code(&self) -> StatusCode {
        match *self {
            UserInsertionError::UserAlreadyExists => StatusCode::FORBIDDEN,
            UserInsertionError::GenericError => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }

    fn error_response(&self) -> HttpResponse {
        error!(
            "An error in the user insertion action has occured: {}",
            self.to_string()
        );
        HttpResponse::build(self.status_code()).body(self.to_string())
    }
}

pub type UserInsertionResult = Result<Uuid, UserInsertionError>;

#[derive(Debug, Error)]
pub enum UserLoginError {
    UserNotFound,
    IncorrectCredentials,
    GenericError,
}

impl Display for UserLoginError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                UserLoginError::UserNotFound => "User not found",
                UserLoginError::IncorrectCredentials => "Incorrect credentials",
                _ => "",
            }
        )
    }
}

impl ResponseError for UserLoginError {
    fn status_code(&self) -> StatusCode {
        match *self {
            UserLoginError::UserNotFound => StatusCode::NOT_FOUND,
            UserLoginError::IncorrectCredentials => StatusCode::FORBIDDEN,
            UserLoginError::GenericError => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }

    fn error_response(&self) -> HttpResponse {
        error!(
            "An error in the user login action has occured: {}",
            self.to_string()
        );
        HttpResponse::build(self.status_code()).body(self.to_string())
    }
}

pub type UserLoginResult = Result<User, UserLoginError>;

#[derive(Debug, Error)]
pub enum UserFindError {
    UserNotFound,
    GenericError,
}

impl Display for UserFindError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                UserFindError::UserNotFound => "User not found",
                _ => "",
            }
        )
    }
}

impl ResponseError for UserFindError {
    fn status_code(&self) -> StatusCode {
        match self {
            UserFindError::UserNotFound => StatusCode::NOT_FOUND,
            UserFindError::GenericError => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }

    fn error_response(&self) -> HttpResponse {
        error!(
            "An error in the user finding action has occured: {}",
            self.to_string()
        );
        HttpResponse::build(self.status_code()).body(self.to_string())
    }
}

pub type UserFindResult = Result<User, UserFindError>;
