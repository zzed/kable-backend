use super::types::RefreshTokenInsertionError;
use crate::diesel::r2d2::{ConnectionManager, PooledConnection};
use crate::diesel::PgConnection;
use crate::diesel::{self, RunQueryDsl};
use crate::models::refresh_token::{NewRefreshToken, RefreshTokenInsertion};
use chrono::{DateTime, Duration, Utc};

type Connection = PooledConnection<ConnectionManager<PgConnection>>;

pub fn insert_refresh_token(
    db: &Connection,
    new_token: RefreshTokenInsertion,
    current_timestamp: DateTime<Utc>,
    duration: Duration,
) -> Result<String, RefreshTokenInsertionError> {
    use crate::schema::refresh_tokens::dsl::*;

    diesel::insert_into(refresh_tokens)
        .values(NewRefreshToken {
            user_id: new_token.user.id,
            token: new_token.token.to_string(),
            created_at: current_timestamp.naive_utc(),
            valid_until: (current_timestamp + duration).naive_utc(),
        })
        .returning(token)
        .get_result::<String>(db)
        .map_err(RefreshTokenInsertionError::DatabaseError)
}
