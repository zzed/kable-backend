use actix_web::http::StatusCode;
use actix_web::{HttpResponse, ResponseError};
use derive_more::Error;
use std::fmt::{Display, Formatter};

#[derive(Debug, Error)]
#[allow(dead_code)]
pub enum RefreshTokenInsertionError {
    DatabaseError(diesel::result::Error),
    GenericError,
}

impl Display for RefreshTokenInsertionError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Refresh token insertion error.")
    }
}

impl ResponseError for RefreshTokenInsertionError {
    fn status_code(&self) -> StatusCode {
        match *self {
            RefreshTokenInsertionError::DatabaseError(_) => StatusCode::INTERNAL_SERVER_ERROR,
            RefreshTokenInsertionError::GenericError => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }

    fn error_response(&self) -> HttpResponse {
        error!(
            "An error in the refresh token insertion action has occured: {}",
            self.to_string()
        );
        HttpResponse::build(self.status_code()).body(self.to_string())
    }
}
