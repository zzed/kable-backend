pub mod actions;
pub mod types;

use super::user::User;
use crate::schema::refresh_tokens;
use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Identifiable, Queryable, Associations, Serialize, Deserialize, PartialEq, Debug)]
#[belongs_to(User)]
#[table_name = "refresh_tokens"]
pub struct RefreshToken {
    pub id: Uuid,
    pub user_id: Uuid,
    pub token: String,
    pub created_at: NaiveDateTime,
    pub valid_until: NaiveDateTime,
}

#[derive(Insertable)]
#[table_name = "refresh_tokens"]
pub struct NewRefreshToken {
    pub user_id: Uuid,
    pub token: String,
    pub created_at: NaiveDateTime,
    pub valid_until: NaiveDateTime,
}

pub struct RefreshTokenInsertion<'a> {
    pub user: Box<User>,
    pub token: &'a str,
}
