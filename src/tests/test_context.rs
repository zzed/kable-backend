use crate::diesel::{Connection, PgConnection, RunQueryDsl};
use crate::embedded_migrations;
pub struct TestContext {
    base_url: String,
    db_name: String,
    pub conn_spec: String,
}

impl TestContext {
    pub fn new(base_url: &str, db_name: &str) -> Self {
        // First, connect to postgres db to be able to create our test database.
        let postgres_url = format!("{}/postgres", base_url);
        let conn =
            PgConnection::establish(&postgres_url).expect("Cannot connect to postgres database.");

        // Create a new database for the test
        let query = diesel::sql_query(format!("CREATE DATABASE {}", db_name).as_str());
        query
            .execute(&conn)
            .unwrap_or_else(|_| panic!("Could not create database {}", db_name));

        let conn = PgConnection::establish(&format!("{}/{}", base_url, db_name))
            .unwrap_or_else(|_| panic!("Cannot connect to {} database", db_name));

        embedded_migrations::run(&conn).unwrap();

        Self {
            base_url: base_url.to_string(),
            db_name: db_name.to_string(),
            conn_spec: format!("{}/{}", base_url, db_name),
        }
    }
}

impl Drop for TestContext {
    fn drop(&mut self) {
        let postgres_url = format!("{}/postgres", self.base_url);
        let conn =
            PgConnection::establish(&postgres_url).expect("Cannot connect to postgres database.");

        let disconnect_users = format!(
            "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = '{}';",
            self.db_name
        );

        diesel::sql_query(disconnect_users.as_str())
            .execute(&conn)
            .unwrap();

        let query = diesel::sql_query(format!("DROP DATABASE {}", self.db_name).as_str());
        query
            .execute(&conn)
            .unwrap_or_else(|_| panic!("Couldn't drop database {}", self.db_name));
    }
}
