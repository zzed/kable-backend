use figment::providers::{Env, Format, Json, Toml, Yaml};
use figment::value::{Dict, Map};
use figment::{Error, Figment, Metadata, Profile, Provider};
use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct ApplicationConfig {
    pub database_url: String,
    pub password_salt: String,
    pub jwt_secret: String,
    pub test_database_url: String,
    pub host: String,
    pub port: u16,
    pub domain: Option<String>,
}

impl Default for ApplicationConfig {
    fn default() -> Self {
        let password_salt: String = thread_rng()
            .sample_iter(&Alphanumeric)
            .take(64)
            .map(char::from)
            .collect();

        let jwt_secret: String = thread_rng()
            .sample_iter(&Alphanumeric)
            .take(64)
            .map(char::from)
            .collect();

        ApplicationConfig {
            database_url: "postgres://kable:kable@localhost/kable".to_string(),
            password_salt,
            jwt_secret,
            test_database_url: "postgres://kable:kable@localhost".to_string(),
            host: "127.0.0.1".to_string(),
            port: 8080,
            domain: None,
        }
    }
}

impl Provider for ApplicationConfig {
    fn metadata(&self) -> Metadata {
        Metadata::named("Application config")
    }

    fn data(&self) -> Result<Map<Profile, Dict>, Error> {
        figment::providers::Serialized::defaults(ApplicationConfig::default()).data()
    }
}

impl ApplicationConfig {
    pub fn figment() -> Figment {
        let figment = Figment::from(ApplicationConfig::default())
            .merge(Json::file("./App.json").nested())
            .merge(Toml::file("./App.toml").nested())
            .merge(Yaml::file("./App.yaml").nested())
            .merge(Env::prefixed("KABLE_").global())
            .merge(Env::raw().only(&["DATABASE_URL"]));
        if !cfg!(debug_assertions) {
            figment.select("release")
        } else if let Ok(profile_name) = std::env::var("PROFILE") {
            figment.select(profile_name)
        } else {
            figment.select("debug")
        }
    }
}
