CREATE TABLE refresh_tokens (
    id uuid DEFAULT gen_random_uuid() PRIMARY KEY,
    user_id uuid NOT NULL,
    token varchar(32) NOT NULL,
    created_at timestamp NOT NULL,
    valid_until timestamp NOT NULL,

    FOREIGN KEY (user_id) REFERENCES users(id)
)