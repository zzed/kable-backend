-- This file should undo anything in `up.sql`

DROP TRIGGER IF EXISTS users_updated_at ON users CASCADE;
DROP TABLE IF EXISTS users CASCADE;