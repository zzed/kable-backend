CREATE TABLE authentications (
    id uuid DEFAULT gen_random_uuid() NOT NULL PRIMARY KEY,
    user_id uuid NOT NULL,
    authentication_type VARCHAR(64) NOT NULL,
    authentication_key VARCHAR(255) NOT NULL,

    FOREIGN KEY (user_id) REFERENCES users(id)
);

INSERT INTO
    authentications (user_id, authentication_type, authentication_key)
    SELECT id, 'password', password_hash
    FROM users;

ALTER TABLE users DROP COLUMN password_hash;