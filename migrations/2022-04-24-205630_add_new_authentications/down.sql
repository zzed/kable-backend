ALTER TABLE users ADD COLUMN password_hash varchar(255);

UPDATE users
SET password_hash=(
    SELECT authentication_key
    FROM authentications
    WHERE
        authentication_type='password' AND authentications.user_id=users.id
);

DROP TABLE IF EXISTS authentications;